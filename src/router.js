import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'Escritorio',
      component: () => import('./views/dashboard/Dashboard.vue')
    },
    {
      path: '/profile',
      name: 'Perfil',
      component: () => import('./views/profile/ProfileHolder.vue'),
      children: [
        {
          path: '',
          name: 'Mi Perfil',
          component: () => import('./views/profile/pages/Profile.vue')
        },
        {
          path: 'avatar',
          name: 'Cambiar avatar',
          component: () => import('./views/profile/pages/Avatar.vue')
        },
        {
          path: 'password',
          name: 'Cambiar contraseña',
          component: () => import('./views/profile/pages/Password.vue')
        }
      ]
    },
    {
      path: '/services',
      name: 'Servicios',
      component: () => import('./views/services/ServicesHolder.vue'),
      children: [
        {
          path: '',
          name: 'Listado servicios',
          component: () => import('./views/services/pages/ServicesAll.vue')
        },
        {
          path: 'new',
          name: 'Nuevo servicio',
          component: () => import('./views/services/pages/ServicesNew.vue')
        },
        {
          path: 'edit/:id',
          name: 'Editar servicio',
          component: () => import('./views/services/pages/ServicesEdit.vue')
        }
      ]
    },
    {
      path: '/customers',
      name: 'Clientes',
      component: () => import('./views/customers/CustomersHolder.vue'),
      children: [
        {
          path: '',
          name: 'Listado clientes',
          component: () => import('./views/customers/pages/CustomersAll.vue')
        },
        {
          path: 'new',
          name: 'Nuevo cliente',
          component: () => import('./views/customers/pages/CustomersNew.vue')
        },
        {
          path: 'edit/:id',
          name: 'Editar cliente',
          component: () => import('./views/customers/pages/CustomersEdit.vue')
        }
      ]
    },
    {
      path: '/providers',
      name: 'Proveedores',
      component: () => import('./views/providers/ProvidersHolder.vue'),
      children: [
        {
          path: '',
          name: 'Listado proveedores',
          component: () => import('./views/providers/pages/ProvidersAll.vue')
        },
        {
          path: 'new',
          name: 'Nuevo proveedor',
          component: () => import('./views/providers/pages/ProvidersNew.vue')
        },
        {
          path: 'edit/:id',
          name: 'Editar proveedor',
          component: () => import('./views/providers/pages/ProvidersEdit.vue')
        }
      ]
    },
    {
      path: '/invoices',
      name: 'Facturas',
      component: () => import('./views/invoices/InvoicesHolder.vue'),
      children: [
        {
          path: '',
          name: 'Listado de facturas',
          component: () => import('./views/invoices/pages/InvoicesAll.vue')
        },
        {
          path: 'new',
          name: 'Nueva factura',
          component: () => import('./views/invoices/pages/InvoicesNew.vue')
        },
        {
          path: 'edit/:id',
          name: 'Editar factura',
          component: () => import('./views/invoices/pages/InvoicesEdit.vue')
        },
        {
          path: 'settings',
          name: 'Configuraciones',
          component: () => import('./views/invoices/pages/InvoicesSettings.vue')
        },
        {
          path: 'reports',
          name: 'Reportes',
          component: () => import('./views/invoices/pages/InvoicesReports.vue')
        }
      ]
    },
    {
      path: '/entries',
      name: 'Contabilidad',
      component: () => import('./views/accounting/AccountingHolder.vue'),
      children: [
        {
          path: '',
          name: 'Partidas contables',
          component: () => import('./views/accounting/pages/AccountingAll.vue')
        },
        {
          path: 'new',
          name: 'Nueva partida',
          component: () => import('./views/accounting/pages/AccountingNew.vue')
        },
        {
          path: 'edit/:id',
          name: 'Editar partida',
          component: () => import('./views/accounting/pages/AccountingEdit.vue')
        },
        {
          path: 'settings',
          name: 'Configuraciones',
          component: () => import('./views/accounting/pages/AccountingSettings.vue')
        },
        {
          path: 'reports',
          name: 'Reportes',
          component: () => import('./views/accounting/pages/AccountingReports.vue')
        }
      ]
    },
    {
      path: '/company',
      name: 'Mi Empresa',
      component: () => import('./views/company/Company.vue')
    }
  ]
})
