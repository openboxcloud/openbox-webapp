import Vue from 'vue'
import Vuex from 'vuex'

const R = require('ramda')
const V = require('voca')

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    isLogged: false,
    user: null,
    cid: null,
    bid: null,
    error: {
      queryDb: 'Ha ocurrido un error al consultar la base de datos. Contacta con tú administrador.'
    },
    idJuridico: "1",
    naturalTaxer: "2"
  },
  mutations: {
    createSession(state, { user, token, bid, cid }) {
      state.isLogged = true
      state.user = user
      state.cid = cid
      state.bid = bid
      localStorage.setItem('token', `${token}`)
    },
    processLogout(state) {
      state.isLogged = false
      state.user = null
      state.cid = null
      state.bid = null
      localStorage.clear()
    },
  },
  actions: {
    processLogin(ctx, data) {
      ctx.commit('createSession', data)
    }
  },
  getters: {
    userAvatar: state => {
      let userAvatar = require('@/assets/images/avatars/main-avatar.png')
      if (!R.isNil(state.user) && !R.isNil(state.user.avatarURL) && !R.isEmpty(state.user.avatarURL)) {
        userAvatar = state.user.avatarURL
      }
      return userAvatar
    },
    userShortName: state => {
      let userShortName = 'Usuario'
      if (!R.isNil(state.user)) {
        const names = V.chain(state.user.names).titleCase().words().value()
        const first = names[0]
        const second = V.first(names[1])
        const last = V.chain(state.user.lastnames).titleCase().words().value()[0]
        userShortName = `${first} ${second}. ${last}`
      }
      return userShortName
    },
    missingProfile: state => R.isNil(state.user) || R.isNil(state.user.profile),
    userProfile: state => !R.isNil(state.user) ? state.user.profile : null,
    userCompanies: state => !R.isNil(state.user) && !R.isNil(state.user.profile) ? state.user.profile.companies : null,
    userBranches: state => !R.isNil(state.user) && !R.isNil(state.user.profile) ? state.user.profile.branches : null,
    modules: state => {
      const modules = [
        {
          path: '/services',
          name: "Servicios",
          icon: `<path d="M 4 4 L 4 5 L 4 23 L 4 24 L 5 24 L 11 24 L 11 22 L 6 22 L 6 6 L 18 6 L 18 7 L 20 7 L 20 5 L 20 4 L 19 4 L 5 4 L 4 4 z M 12 8 L 12 9 L 12 27 L 12 28 L 13 28 L 27 28 L 28 28 L 28 27 L 28 9 L 28 8 L 27 8 L 13 8 L 12 8 z M 14 10 L 26 10 L 26 26 L 14 26 L 14 10 z"/>`
        },
        {
          path: '/invoices',
          name: "Facturación",
          icon: `<path d="M 6 3 L 6 4 L 6 28 L 6 29 L 7 29 L 25 29 L 26 29 L 26 28 L 26 10 L 26 9.59375 L 25.71875 9.28125 L 19.71875 3.28125 L 19.40625 3 L 19 3 L 7 3 L 6 3 z M 8 5 L 18 5 L 18 10 L 18 11 L 19 11 L 24 11 L 24 27 L 8 27 L 8 5 z M 20 6.4375 L 22.5625 9 L 20 9 L 20 6.4375 z M 11 13 L 11 15 L 21 15 L 21 13 L 11 13 z M 11 17 L 11 19 L 21 19 L 21 17 L 11 17 z M 11 21 L 11 23 L 21 23 L 21 21 L 11 21 z"/>`
        },
        {
          path: '/customers',
          name: "Clientes",
          icon: `<path d="M 9 7 C 5.6981362 7 3 9.6981359 3 13 C 3 14.983743 3.9751342 16.750036 5.46875 17.84375 C 2.831147 19.154076 1 21.864226 1 25 L 3 25 C 3 21.674438 5.674438 19 9 19 C 12.325562 19 15 21.674438 15 25 L 17 25 C 17 21.674438 19.674438 19 23 19 C 26.325562 19 29 21.674438 29 25 L 31 25 C 31 21.864226 29.168853 19.154076 26.53125 17.84375 C 28.024866 16.750036 29 14.983743 29 13 C 29 9.698136 26.301864 7 23 7 C 19.698136 7 17 9.698136 17 13 C 17 14.983743 17.975134 16.750036 19.46875 17.84375 C 18.010284 18.568297 16.788984 19.705335 16 21.125 C 15.211016 19.705335 13.989716 18.568297 12.53125 17.84375 C 14.024866 16.750036 15 14.983743 15 13 C 15 9.698136 12.301864 7 9 7 z M 9 9 C 11.220984 9 13 10.779016 13 13 C 13 15.220984 11.220984 17 9 17 C 6.7790164 17 5 15.220984 5 13 C 5 10.779016 6.7790164 9 9 9 z M 23 9 C 25.220984 9 27 10.779016 27 13 C 27 15.220984 25.220984 17 23 17 C 20.779016 17 19 15.220984 19 13 C 19 10.779016 20.779016 9 23 9 z"/>`
        },
        {
          path: '/providers',
          name: "Proveedores",
          icon: `<path d="M 20 5 L 20 6 L 20 10.46875 L 17 12.25 L 17 11 L 17 9.21875 L 15.5 10.15625 L 12 12.25 L 12 11 L 12 9.21875 L 10.5 10.15625 L 5.5 13.15625 L 5 13.4375 L 5 14 L 5 26 L 5 27 L 6 27 L 11 27 L 13 27 L 18 27 L 22 27 L 26 27 L 27 27 L 27 26 L 27 6 L 27 5 L 26 5 L 21 5 L 20 5 z M 22 7 L 25 7 L 25 25 L 22 25 L 18 25 L 13 25 L 11 25 L 7 25 L 7 14.53125 L 10 12.75 L 10 14 L 10 15.78125 L 11.5 14.84375 L 15 12.75 L 15 14 L 15 15.78125 L 16.5 14.84375 L 21.5 11.84375 L 22 11.5625 L 22 11 L 22 7 z M 9 17 L 9 19 L 11 19 L 11 17 L 9 17 z M 13 17 L 13 19 L 15 19 L 15 17 L 13 17 z M 17 17 L 17 19 L 19 19 L 19 17 L 17 17 z M 21 17 L 21 19 L 23 19 L 23 17 L 21 17 z M 9 21 L 9 23 L 11 23 L 11 21 L 9 21 z M 13 21 L 13 23 L 15 23 L 15 21 L 13 21 z M 17 21 L 17 23 L 19 23 L 19 21 L 17 21 z M 21 21 L 21 23 L 23 23 L 23 21 L 21 21 z"/>`
        },
        {
          path: '/inventory',
          name: "Inventarios",
          icon: `<path d="M 16 3 C 14.74096 3 13.84816 3.8903886 13.40625 5 L 11 5 L 10 5 L 7 5 L 6 5 L 6 6 L 6 27 L 6 28 L 7 28 L 25 28 L 26 28 L 26 27 L 26 6 L 26 5 L 25 5 L 22 5 L 21 5 L 18.59375 5 C 18.15184 3.8903886 17.25904 3 16 3 z M 16 5 C 16.554545 5 17 5.4454545 17 6 L 17 7 L 18 7 L 20 7 L 20 9 L 12 9 L 12 7 L 14 7 L 15 7 L 15 6 C 15 5.4454545 15.445455 5 16 5 z M 8 7 L 10 7 L 10 10 L 10 11 L 11 11 L 21 11 L 22 11 L 22 10 L 22 7 L 24 7 L 24 26 L 8 26 L 8 7 z"/>`
        },
        {
          path: '/entries',
          name: "Contabilidad",
          icon: `<path d="M 27.9375 4.75 L 3.15625 9 L 15 9 L 26.3125 7.0625 L 26.65625 9 L 28.6875 9 L 27.9375 4.75 z M 2 10 L 2 11 L 2 12.5 L 2 25 L 2 26 L 3 26 L 29 26 L 30 26 L 30 25 L 30 11 L 30 10 L 29 10 L 4.5 10 L 3 10 L 2 10 z M 6.9375 12 L 25.0625 12 C 25.029198 12.162744 25 12.327411 25 12.5 C 25 13.880712 26.119288 15 27.5 15 C 27.672589 15 27.837256 14.970802 28 14.9375 L 28 21.0625 C 27.837256 21.029198 27.672589 21 27.5 21 C 26.119288 21 25 22.119288 25 23.5 C 25 23.672589 25.029198 23.837256 25.0625 24 L 6.9375 24 C 6.9708022 23.837256 7 23.672589 7 23.5 C 7 22.119288 5.8807119 21 4.5 21 C 4.327411 21 4.162744 21.029198 4 21.0625 L 4 14.9375 C 4.162744 14.970802 4.327411 15 4.5 15 C 5.8807119 15 7 13.880712 7 12.5 C 7 12.327411 6.9708022 12.162744 6.9375 12 z M 16 13 C 13.250421 13 11 15.250421 11 18 C 11 20.749579 13.250421 23 16 23 C 18.749579 23 21 20.749579 21 18 C 21 15.250421 18.749579 13 16 13 z M 16 15 C 17.668699 15 19 16.331301 19 18 C 19 19.668699 17.668699 21 16 21 C 14.331301 21 13 19.668699 13 18 C 13 16.331301 14.331301 15 16 15 z"/>`
        },
        {
          path: '/companies',
          name: "Empresas",
          icon: `<path d="M 4 3 L 4 4 L 4 28 L 4 29 L 5 29 L 14 29 L 15 29 L 15 28 L 15 25 L 17 25 L 17 28 L 17 29 L 18 29 L 27 29 L 28 29 L 28 28 L 28 4 L 28 3 L 27 3 L 5 3 L 4 3 z M 6 5 L 26 5 L 26 27 L 19 27 L 19 24 L 19 23 L 18 23 L 14 23 L 13 23 L 13 24 L 13 27 L 6 27 L 6 5 z M 8 7 L 8 9 L 12 9 L 12 7 L 8 7 z M 14 7 L 14 9 L 18 9 L 18 7 L 14 7 z M 20 7 L 20 9 L 24 9 L 24 7 L 20 7 z M 8 11 L 8 13 L 12 13 L 12 11 L 8 11 z M 14 11 L 14 13 L 18 13 L 18 11 L 14 11 z M 20 11 L 20 13 L 24 13 L 24 11 L 20 11 z M 8 15 L 8 17 L 12 17 L 12 15 L 8 15 z M 14 15 L 14 17 L 18 17 L 18 15 L 14 15 z M 20 15 L 20 17 L 24 17 L 24 15 L 20 15 z M 8 19 L 8 21 L 12 21 L 12 19 L 8 19 z M 14 19 L 14 21 L 18 21 L 18 19 L 14 19 z M 20 19 L 20 21 L 24 21 L 24 19 L 20 19 z M 8 23 L 8 25 L 12 25 L 12 23 L 8 23 z M 20 23 L 20 25 L 24 25 L 24 23 L 20 23 z"/>`
        }
      ]
      return state.user.profile.accesses
        .filter(access => {
          return access.company.id === state.cid && access.branch.id === state.bid
        })
        .map(access => {
          const baseModule = modules.find(module => module.name.toLowerCase() === access.module.name.toLowerCase())
          return {
            id: access.id,
            moduleId: access.module.id,
            moduleName: access.module.name,
            permissions: access.permissions,
            ...baseModule
          }
        })
    },
    bid: state => state.bid,
    cid: state => state.cid,
    currentCompany: (state, getters) => R.find(R.propEq('id', state.cid))(getters.userCompanies),
    currentBranch: (state, getters) => R.find(R.propEq('id', state.bid))(getters.userBranches),
    user: state => state.user
  }
})
