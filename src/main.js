import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import axios from "axios";
import * as R from "ramda";
import { formatMoney } from "./utils";

import ElementUI from "element-ui";
import locale from "element-ui/lib/locale/lang/es";
import VueTheMask from "vue-the-mask";

// Styles
import "@/assets/css/main.css";
import "@/assets/scss/elemets-variables.scss";
import "@/assets/less/font-awesome.less";

Vue.config.productionTip = false;
Vue.use(ElementUI, { locale });
Vue.use(require("vue-shortkey"));
Vue.use(VueTheMask);

Vue.filter("formatMoney", function(value) {
  return formatMoney(value);
});
Vue.filter("formatDate", function(value, fromFormat = null) {
  const options = { year: "numeric", month: "2-digit", day: "2-digit" };
  if (!R.isNil(value)) {
    switch (fromFormat) {
      case "YYYY-MM-DD":
        return new Intl.DateTimeFormat("es-ES", options).format(
          new Date(value).setDate(new Date(value).getDate() + 1)
        );
        break;
      default:
        const date = value
          .split("/")
          .reverse()
          .join("-");
        return new Intl.DateTimeFormat("es-ES").format(new Date(date));
        break;
    }
  } else {
    return "";
  }
});

axios.interceptors.request.use(config => {
  config.baseURL =
    process.env.NODE_ENV == "development"
      ? "http://localhost:4000"
      : "https://api2.openbox.cloud";
  // config.baseURL = "https://api.openbox.cloud";
  config.headers.common["Authorization"] = `Bearer ${localStorage.getItem(
    "token"
  )}`;
  return config;
});

axios.interceptors.response.use(res => {
  // Valida si no se recibe un mensaje de sesion de invalido.
  if (!R.isNil(res.data.errors)) {
    const response = res.data.errors[0].message;
    let message;
    try {
      message = JSON.parse(response);
    } catch (error) {
      message = response;
    }
    if (!R.isNil(message.isValid) && !message.isValid) {
      store.commit("processLogout");
      return false;
    }
  }
  return res;
});

router.afterEach((to, from) => {
  NProgress.done();
});

const main = async () => {
  if (!R.isNil(localStorage.getItem("token"))) {
    const res = await axios.post("/", {
      query: `
          mutation {
            validateSession {
              isValid
              response {
                token
                cid
                bid
                user {
                  id
                  unique
                  names
                  lastnames
                  email
                  changePassword
                  avatarURL
                  profile {
                    id
                    name
                    companies { id name }
                    branches { id name }
                    accesses {
                      id
                      permissions
                      module { id name }
                      company { id name }
                      branch { id name }
                    }
                  }
                }
              }
            }
          }`
    });
    const data = res.data.data;
    if (!R.isNil(data)) {
      store.dispatch("processLogin", data.validateSession.response);
    }
  }
  new Vue({
    router,
    store,
    render: h => h(App)
  }).$mount("#app");
};
main();
